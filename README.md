# Uygulama

> Vue.js ile hazırlanmış basit bir TODO uygulamasıdır.  

## Kurulum

``` bash
# Bağımlılıkların Yüklenmesi
npm install

# Varsayılan yükleme açılma adresi localhost:8080
npm run dev

# Üretim ortamına derleme
npm run build
```
___
## Uygulama Geliştirme Amacı
Web Tasarım Ödevi İçin Geliştirilen Basit Bir Todo Uygulaması
Fatih Çalışan - 1185613004
