import Vue from "vue";
import App from "./App.vue";

// Components
import TodoList from "./Components/List.vue";
import AddTodo from "./Components/AddTodo.vue";
import CompletedTodo from "./Components/CompletedTodo.vue";

Vue.component("todo-list", TodoList);
Vue.component("add-todo", AddTodo);
Vue.component("completed-todo", CompletedTodo);

// Event Bus
export const eventBus = new Vue();

new Vue({
  el: "#app",
  render: h => h(App)
});
